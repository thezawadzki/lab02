// $ANTLR 3.4 /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g 2021-03-10 22:34:31

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class ExprParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ID", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "PRINT", "RP", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ID=5;
    public static final int INT=6;
    public static final int LP=7;
    public static final int MINUS=8;
    public static final int MUL=9;
    public static final int NL=10;
    public static final int PLUS=11;
    public static final int PODST=12;
    public static final int PRINT=13;
    public static final int RP=14;
    public static final int VAR=15;
    public static final int WS=16;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public ExprParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public ExprParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return ExprParser.tokenNames; }
    public String getGrammarFileName() { return "/home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g"; }


    public static class prog_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "prog"
    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:16:1: prog : ( stat )+ EOF !;
    public final ExprParser.prog_return prog() throws RecognitionException {
        ExprParser.prog_return retval = new ExprParser.prog_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        ExprParser.stat_return stat1 =null;


        CommonTree EOF2_tree=null;

        try {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:5: ( ( stat )+ EOF !)
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+ EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= ID && LA1_0 <= LP)||LA1_0==NL||LA1_0==PRINT||LA1_0==VAR) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:17:8: stat
            	    {
            	    pushFollow(FOLLOW_stat_in_prog49);
            	    stat1=stat();

            	    state._fsp--;

            	    adaptor.addChild(root_0, stat1.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_prog54); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class stat_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "stat"
    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:19:1: stat : ( expr NL -> expr | PRINT ^ expr | ID PODST expr NL -> ^( PODST ID expr ) | VAR ID NL -> ^( VAR ID ) | VAR ID PODST expr NL -> ^( PODST VAR ID expr ) | NL ->);
    public final ExprParser.stat_return stat() throws RecognitionException {
        ExprParser.stat_return retval = new ExprParser.stat_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token NL4=null;
        Token PRINT5=null;
        Token ID7=null;
        Token PODST8=null;
        Token NL10=null;
        Token VAR11=null;
        Token ID12=null;
        Token NL13=null;
        Token VAR14=null;
        Token ID15=null;
        Token PODST16=null;
        Token NL18=null;
        Token NL19=null;
        ExprParser.expr_return expr3 =null;

        ExprParser.expr_return expr6 =null;

        ExprParser.expr_return expr9 =null;

        ExprParser.expr_return expr17 =null;


        CommonTree NL4_tree=null;
        CommonTree PRINT5_tree=null;
        CommonTree ID7_tree=null;
        CommonTree PODST8_tree=null;
        CommonTree NL10_tree=null;
        CommonTree VAR11_tree=null;
        CommonTree ID12_tree=null;
        CommonTree NL13_tree=null;
        CommonTree VAR14_tree=null;
        CommonTree ID15_tree=null;
        CommonTree PODST16_tree=null;
        CommonTree NL18_tree=null;
        CommonTree NL19_tree=null;
        RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
        RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        try {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:20:5: ( expr NL -> expr | PRINT ^ expr | ID PODST expr NL -> ^( PODST ID expr ) | VAR ID NL -> ^( VAR ID ) | VAR ID PODST expr NL -> ^( PODST VAR ID expr ) | NL ->)
            int alt2=6;
            switch ( input.LA(1) ) {
            case INT:
            case LP:
                {
                alt2=1;
                }
                break;
            case ID:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==PODST) ) {
                    alt2=3;
                }
                else if ( (LA2_2==DIV||(LA2_2 >= MINUS && LA2_2 <= PLUS)) ) {
                    alt2=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;

                }
                }
                break;
            case PRINT:
                {
                alt2=2;
                }
                break;
            case VAR:
                {
                int LA2_4 = input.LA(2);

                if ( (LA2_4==ID) ) {
                    int LA2_7 = input.LA(3);

                    if ( (LA2_7==NL) ) {
                        alt2=4;
                    }
                    else if ( (LA2_7==PODST) ) {
                        alt2=5;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 7, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 4, input);

                    throw nvae;

                }
                }
                break;
            case NL:
                {
                alt2=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:20:7: expr NL
                    {
                    pushFollow(FOLLOW_expr_in_stat67);
                    expr3=expr();

                    state._fsp--;

                    stream_expr.add(expr3.getTree());

                    NL4=(Token)match(input,NL,FOLLOW_NL_in_stat69);  
                    stream_NL.add(NL4);


                    // AST REWRITE
                    // elements: expr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 20:15: -> expr
                    {
                        adaptor.addChild(root_0, stream_expr.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:22:7: PRINT ^ expr
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    PRINT5=(Token)match(input,PRINT,FOLLOW_PRINT_in_stat82); 
                    PRINT5_tree = 
                    (CommonTree)adaptor.create(PRINT5)
                    ;
                    root_0 = (CommonTree)adaptor.becomeRoot(PRINT5_tree, root_0);


                    pushFollow(FOLLOW_expr_in_stat85);
                    expr6=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr6.getTree());

                    }
                    break;
                case 3 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:7: ID PODST expr NL
                    {
                    ID7=(Token)match(input,ID,FOLLOW_ID_in_stat93);  
                    stream_ID.add(ID7);


                    PODST8=(Token)match(input,PODST,FOLLOW_PODST_in_stat95);  
                    stream_PODST.add(PODST8);


                    pushFollow(FOLLOW_expr_in_stat97);
                    expr9=expr();

                    state._fsp--;

                    stream_expr.add(expr9.getTree());

                    NL10=(Token)match(input,NL,FOLLOW_NL_in_stat99);  
                    stream_NL.add(NL10);


                    // AST REWRITE
                    // elements: expr, PODST, ID
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 23:24: -> ^( PODST ID expr )
                    {
                        // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:23:27: ^( PODST ID expr )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_PODST.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_1, stream_expr.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:24:7: VAR ID NL
                    {
                    VAR11=(Token)match(input,VAR,FOLLOW_VAR_in_stat117);  
                    stream_VAR.add(VAR11);


                    ID12=(Token)match(input,ID,FOLLOW_ID_in_stat119);  
                    stream_ID.add(ID12);


                    NL13=(Token)match(input,NL,FOLLOW_NL_in_stat121);  
                    stream_NL.add(NL13);


                    // AST REWRITE
                    // elements: VAR, ID
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 24:17: -> ^( VAR ID )
                    {
                        // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:24:20: ^( VAR ID )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_VAR.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 5 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:7: VAR ID PODST expr NL
                    {
                    VAR14=(Token)match(input,VAR,FOLLOW_VAR_in_stat137);  
                    stream_VAR.add(VAR14);


                    ID15=(Token)match(input,ID,FOLLOW_ID_in_stat139);  
                    stream_ID.add(ID15);


                    PODST16=(Token)match(input,PODST,FOLLOW_PODST_in_stat141);  
                    stream_PODST.add(PODST16);


                    pushFollow(FOLLOW_expr_in_stat143);
                    expr17=expr();

                    state._fsp--;

                    stream_expr.add(expr17.getTree());

                    NL18=(Token)match(input,NL,FOLLOW_NL_in_stat145);  
                    stream_NL.add(NL18);


                    // AST REWRITE
                    // elements: PODST, VAR, ID, expr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 25:28: -> ^( PODST VAR ID expr )
                    {
                        // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:25:31: ^( PODST VAR ID expr )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_PODST.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_VAR.nextNode()
                        );

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_1, stream_expr.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 6 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:27:7: NL
                    {
                    NL19=(Token)match(input,NL,FOLLOW_NL_in_stat166);  
                    stream_NL.add(NL19);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 27:10: ->
                    {
                        root_0 = null;
                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "stat"


    public static class expr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expr"
    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:30:1: expr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* ;
    public final ExprParser.expr_return expr() throws RecognitionException {
        ExprParser.expr_return retval = new ExprParser.expr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token PLUS21=null;
        Token MINUS23=null;
        ExprParser.multExpr_return multExpr20 =null;

        ExprParser.multExpr_return multExpr22 =null;

        ExprParser.multExpr_return multExpr24 =null;


        CommonTree PLUS21_tree=null;
        CommonTree MINUS23_tree=null;

        try {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:31:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* )
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:31:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multExpr_in_expr185);
            multExpr20=multExpr();

            state._fsp--;

            adaptor.addChild(root_0, multExpr20.getTree());

            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:7: ( PLUS ^ multExpr | MINUS ^ multExpr )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==PLUS) ) {
                    alt3=1;
                }
                else if ( (LA3_0==MINUS) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:32:9: PLUS ^ multExpr
            	    {
            	    PLUS21=(Token)match(input,PLUS,FOLLOW_PLUS_in_expr195); 
            	    PLUS21_tree = 
            	    (CommonTree)adaptor.create(PLUS21)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(PLUS21_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_expr198);
            	    multExpr22=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr22.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:33:9: MINUS ^ multExpr
            	    {
            	    MINUS23=(Token)match(input,MINUS,FOLLOW_MINUS_in_expr208); 
            	    MINUS23_tree = 
            	    (CommonTree)adaptor.create(MINUS23)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MINUS23_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_expr211);
            	    multExpr24=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr24.getTree());

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"


    public static class multExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multExpr"
    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:37:1: multExpr : atom ( MUL ^ atom | DIV ^ atom )* ;
    public final ExprParser.multExpr_return multExpr() throws RecognitionException {
        ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token MUL26=null;
        Token DIV28=null;
        ExprParser.atom_return atom25 =null;

        ExprParser.atom_return atom27 =null;

        ExprParser.atom_return atom29 =null;


        CommonTree MUL26_tree=null;
        CommonTree DIV28_tree=null;

        try {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:5: ( atom ( MUL ^ atom | DIV ^ atom )* )
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:38:7: atom ( MUL ^ atom | DIV ^ atom )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_atom_in_multExpr237);
            atom25=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom25.getTree());

            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:7: ( MUL ^ atom | DIV ^ atom )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==MUL) ) {
                    alt4=1;
                }
                else if ( (LA4_0==DIV) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:39:9: MUL ^ atom
            	    {
            	    MUL26=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr247); 
            	    MUL26_tree = 
            	    (CommonTree)adaptor.create(MUL26)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MUL26_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr250);
            	    atom27=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom27.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:40:9: DIV ^ atom
            	    {
            	    DIV28=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr260); 
            	    DIV28_tree = 
            	    (CommonTree)adaptor.create(DIV28)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(DIV28_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr263);
            	    atom29=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom29.getTree());

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multExpr"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:44:1: atom : ( INT | ID | LP ! expr RP !);
    public final ExprParser.atom_return atom() throws RecognitionException {
        ExprParser.atom_return retval = new ExprParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INT30=null;
        Token ID31=null;
        Token LP32=null;
        Token RP34=null;
        ExprParser.expr_return expr33 =null;


        CommonTree INT30_tree=null;
        CommonTree ID31_tree=null;
        CommonTree LP32_tree=null;
        CommonTree RP34_tree=null;

        try {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:45:5: ( INT | ID | LP ! expr RP !)
            int alt5=3;
            switch ( input.LA(1) ) {
            case INT:
                {
                alt5=1;
                }
                break;
            case ID:
                {
                alt5=2;
                }
                break;
            case LP:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }

            switch (alt5) {
                case 1 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:45:7: INT
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    INT30=(Token)match(input,INT,FOLLOW_INT_in_atom289); 
                    INT30_tree = 
                    (CommonTree)adaptor.create(INT30)
                    ;
                    adaptor.addChild(root_0, INT30_tree);


                    }
                    break;
                case 2 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:46:7: ID
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    ID31=(Token)match(input,ID,FOLLOW_ID_in_atom297); 
                    ID31_tree = 
                    (CommonTree)adaptor.create(ID31)
                    ;
                    adaptor.addChild(root_0, ID31_tree);


                    }
                    break;
                case 3 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/Expr.g:47:7: LP ! expr RP !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    LP32=(Token)match(input,LP,FOLLOW_LP_in_atom305); 

                    pushFollow(FOLLOW_expr_in_atom308);
                    expr33=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr33.getTree());

                    RP34=(Token)match(input,RP,FOLLOW_RP_in_atom310); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"

    // Delegated rules


 

    public static final BitSet FOLLOW_stat_in_prog49 = new BitSet(new long[]{0x000000000000A4E0L});
    public static final BitSet FOLLOW_EOF_in_prog54 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_stat67 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat69 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PRINT_in_stat82 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_expr_in_stat85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stat93 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_PODST_in_stat95 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_expr_in_stat97 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat99 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_stat117 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_stat119 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_stat137 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_stat139 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_PODST_in_stat141 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_expr_in_stat143 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_NL_in_stat145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NL_in_stat166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_multExpr_in_expr185 = new BitSet(new long[]{0x0000000000000902L});
    public static final BitSet FOLLOW_PLUS_in_expr195 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_multExpr_in_expr198 = new BitSet(new long[]{0x0000000000000902L});
    public static final BitSet FOLLOW_MINUS_in_expr208 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_multExpr_in_expr211 = new BitSet(new long[]{0x0000000000000902L});
    public static final BitSet FOLLOW_atom_in_multExpr237 = new BitSet(new long[]{0x0000000000000212L});
    public static final BitSet FOLLOW_MUL_in_multExpr247 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_atom_in_multExpr250 = new BitSet(new long[]{0x0000000000000212L});
    public static final BitSet FOLLOW_DIV_in_multExpr260 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_atom_in_multExpr263 = new BitSet(new long[]{0x0000000000000212L});
    public static final BitSet FOLLOW_INT_in_atom289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_atom297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LP_in_atom305 = new BitSet(new long[]{0x00000000000000E0L});
    public static final BitSet FOLLOW_expr_in_atom308 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_RP_in_atom310 = new BitSet(new long[]{0x0000000000000002L});

}