// $ANTLR 3.4 /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g 2021-03-10 22:34:32

package tb.antlr.interpreter;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class TExpr1 extends MyTreeParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ID", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "PRINT", "RP", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ID=5;
    public static final int INT=6;
    public static final int LP=7;
    public static final int MINUS=8;
    public static final int MUL=9;
    public static final int NL=10;
    public static final int PLUS=11;
    public static final int PODST=12;
    public static final int PRINT=13;
    public static final int RP=14;
    public static final int VAR=15;
    public static final int WS=16;

    // delegates
    public MyTreeParser[] getDelegates() {
        return new MyTreeParser[] {};
    }

    // delegators


    public TExpr1(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public TExpr1(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return TExpr1.tokenNames; }
    public String getGrammarFileName() { return "/home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g"; }



    // $ANTLR start "prog"
    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:1: prog : (e= expr )* ;
    public final void prog() throws RecognitionException {
        TExpr1.expr_return e =null;


        try {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:9: ( (e= expr )* )
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:11: (e= expr )*
            {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:11: (e= expr )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= DIV && LA1_0 <= INT)||(LA1_0 >= MINUS && LA1_0 <= MUL)||(LA1_0 >= PLUS && LA1_0 <= PODST)||LA1_0==VAR) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:14:12: e= expr
            	    {
            	    pushFollow(FOLLOW_expr_in_prog52);
            	    e=expr();

            	    state._fsp--;


            	    drukuj ((e!=null?(input.getTokenStream().toString(input.getTreeAdaptor().getTokenStartIndex(e.start),input.getTreeAdaptor().getTokenStopIndex(e.start))):null) + " = " + (e!=null?e.out:null).toString());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "prog"


    public static class expr_return extends TreeRuleReturnScope {
        public Integer out;
    };


    // $ANTLR start "expr"
    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:16:1: expr returns [Integer out] : ( ^( PLUS e1= expr e2= expr ) | ^( MINUS e1= expr e2= expr ) | ^( MUL e1= expr e2= expr ) | ^( DIV e1= expr e2= expr ) | ^( PODST i1= ID e2= expr ) | ^( VAR i1= ID ) | ^( PODST VAR i1= ID e2= expr ) | ID | INT );
    public final TExpr1.expr_return expr() throws RecognitionException {
        TExpr1.expr_return retval = new TExpr1.expr_return();
        retval.start = input.LT(1);


        CommonTree i1=null;
        CommonTree ID1=null;
        CommonTree INT2=null;
        TExpr1.expr_return e1 =null;

        TExpr1.expr_return e2 =null;


        try {
            // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:17:8: ( ^( PLUS e1= expr e2= expr ) | ^( MINUS e1= expr e2= expr ) | ^( MUL e1= expr e2= expr ) | ^( DIV e1= expr e2= expr ) | ^( PODST i1= ID e2= expr ) | ^( VAR i1= ID ) | ^( PODST VAR i1= ID e2= expr ) | ID | INT )
            int alt2=9;
            switch ( input.LA(1) ) {
            case PLUS:
                {
                alt2=1;
                }
                break;
            case MINUS:
                {
                alt2=2;
                }
                break;
            case MUL:
                {
                alt2=3;
                }
                break;
            case DIV:
                {
                alt2=4;
                }
                break;
            case PODST:
                {
                int LA2_5 = input.LA(2);

                if ( (LA2_5==DOWN) ) {
                    int LA2_9 = input.LA(3);

                    if ( (LA2_9==ID) ) {
                        alt2=5;
                    }
                    else if ( (LA2_9==VAR) ) {
                        alt2=7;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 9, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 5, input);

                    throw nvae;

                }
                }
                break;
            case VAR:
                {
                alt2=6;
                }
                break;
            case ID:
                {
                alt2=8;
                }
                break;
            case INT:
                {
                alt2=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:17:10: ^( PLUS e1= expr e2= expr )
                    {
                    match(input,PLUS,FOLLOW_PLUS_in_expr76); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr81);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr85);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = add((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 2 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:18:11: ^( MINUS e1= expr e2= expr )
                    {
                    match(input,MINUS,FOLLOW_MINUS_in_expr102); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr106);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr110);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = sub((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 3 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:19:11: ^( MUL e1= expr e2= expr )
                    {
                    match(input,MUL,FOLLOW_MUL_in_expr127); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr133);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr137);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = mul((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 4 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:20:11: ^( DIV e1= expr e2= expr )
                    {
                    match(input,DIV,FOLLOW_DIV_in_expr154); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr160);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr164);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = div((e1!=null?e1.out:null), (e2!=null?e2.out:null));

                    }
                    break;
                case 5 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:21:11: ^( PODST i1= ID e2= expr )
                    {
                    match(input,PODST,FOLLOW_PODST_in_expr181); 

                    match(input, Token.DOWN, null); 
                    i1=(CommonTree)match(input,ID,FOLLOW_ID_in_expr185); 

                    pushFollow(FOLLOW_expr_in_expr191);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = setVariable((i1!=null?i1.getText():null), (e2!=null?e2.out:null));

                    }
                    break;
                case 6 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:22:11: ^( VAR i1= ID )
                    {
                    match(input,VAR,FOLLOW_VAR_in_expr208); 

                    match(input, Token.DOWN, null); 
                    i1=(CommonTree)match(input,ID,FOLLOW_ID_in_expr212); 

                    match(input, Token.UP, null); 


                    retval.out = init((i1!=null?i1.getText():null));

                    }
                    break;
                case 7 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:23:11: ^( PODST VAR i1= ID e2= expr )
                    {
                    match(input,PODST,FOLLOW_PODST_in_expr241); 

                    match(input, Token.DOWN, null); 
                    match(input,VAR,FOLLOW_VAR_in_expr243); 

                    i1=(CommonTree)match(input,ID,FOLLOW_ID_in_expr247); 

                    pushFollow(FOLLOW_expr_in_expr251);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    retval.out = init((i1!=null?i1.getText():null), (e2!=null?e2.out:null));

                    }
                    break;
                case 8 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:24:11: ID
                    {
                    ID1=(CommonTree)match(input,ID,FOLLOW_ID_in_expr265); 

                    retval.out = getValue((ID1!=null?ID1.getText():null));

                    }
                    break;
                case 9 :
                    // /home/student/git/antlr_swing/antlr_swing/src/tb/antlr/interpreter/TExpr1.g:25:11: INT
                    {
                    INT2=(CommonTree)match(input,INT,FOLLOW_INT_in_expr302); 

                    retval.out = getInt((INT2!=null?INT2.getText():null));

                    }
                    break;

            }
        }
        catch (RuntimeException ex) {
            printEx(ex);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"

    // Delegated rules


 

    public static final BitSet FOLLOW_expr_in_prog52 = new BitSet(new long[]{0x0000000000009B72L});
    public static final BitSet FOLLOW_PLUS_in_expr76 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr81 = new BitSet(new long[]{0x0000000000009B70L});
    public static final BitSet FOLLOW_expr_in_expr85 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MINUS_in_expr102 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr106 = new BitSet(new long[]{0x0000000000009B70L});
    public static final BitSet FOLLOW_expr_in_expr110 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MUL_in_expr127 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr133 = new BitSet(new long[]{0x0000000000009B70L});
    public static final BitSet FOLLOW_expr_in_expr137 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DIV_in_expr154 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr160 = new BitSet(new long[]{0x0000000000009B70L});
    public static final BitSet FOLLOW_expr_in_expr164 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PODST_in_expr181 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_expr185 = new BitSet(new long[]{0x0000000000009B70L});
    public static final BitSet FOLLOW_expr_in_expr191 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_VAR_in_expr208 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_expr212 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PODST_in_expr241 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_VAR_in_expr243 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_expr247 = new BitSet(new long[]{0x0000000000009B70L});
    public static final BitSet FOLLOW_expr_in_expr251 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_ID_in_expr265 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_expr302 = new BitSet(new long[]{0x0000000000000002L});

}