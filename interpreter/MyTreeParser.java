package tb.antlr.interpreter;

import java.util.HashMap;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {
	protected HashMap<String, Integer> map = new HashMap<String, Integer>();
	
	protected GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a+b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a-b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a*b;
	}
	
	protected Integer div(Integer a, Integer b) {
		if(b==0) {
			throw new RuntimeException("Division by zero exception");
		}
		return a/b;
	}
	
	protected Integer init(String i1) {
		return this.init(i1, 0);
	}
	
	protected Integer init(String name, Integer val) {
		if(map.containsKey(name)) {
			throw new RuntimeException("Variable " + name + " already initialied");
		}
		map.put(name,  val);
		return val;
	}
	
	protected void printEx(RuntimeException ex) {
		System.out.println(ex.getMessage());
	}
	
	protected Integer setVariable(String name, Integer val) {
		if(map.containsKey(name)) {
			map.put(name, val);
			return val;
		}
		throw new RuntimeException("Variable " + name + " not initialized");
	}
	
	protected void createVariable(String name) {
		globalSymbols.newSymbol(name);
	}
	
	protected Integer getValue(String name) {
		if(map.containsKey(name)) {
			return map.get(name);
		}
		throw new RuntimeException("Variable " + name + " doesn't exist");
	}
	
/* USING GET SYMBOLS */	
//	protected Integer getValue(String name) {
//		return globalSymbols.getSymbol(name);
//	}
	
//	protected Integer setVariable(String name, Integer val) {
//	globalSymbols.setSymbol(name, val);
//	return val;
//}

	
}
